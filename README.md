# Beli Aja Web API

Aplikasi ini hanya aplikasi API Service. Aplikasi ini memiliki desain database seperti berikut.

![alt text](screenshot/Selection_001.png)

Dan berikut adalah hasil JSON dari request API yang diberikan

![alt text](screenshot/Selection_002.png)


# Mobile Apps

Untuk clientnya berbentuk android apps, apk ada pada folder apk. Ketika melakukan installasi pastikan komputer yang menjadi server dari Web API Beli Aja memiliki IP : 192.168.100.64