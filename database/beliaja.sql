-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 03, 2019 at 02:00 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beliaja`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE `tbl_cart` (
  `cart_id` int(11) NOT NULL,
  `pembeli_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL DEFAULT '1',
  `notes` varchar(200) DEFAULT NULL,
  `date_use` datetime DEFAULT NULL,
  `status_prod` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cart`
--

INSERT INTO `tbl_cart` (`cart_id`, `pembeli_id`, `produk_id`, `jumlah`, `notes`, `date_use`, `status_prod`) VALUES
(1, 1, 27, 1, '', '2019-03-24 21:59:31', 0),
(2, 1, 33, 1, '', '2019-03-24 21:59:59', 0),
(3, 1, 13, 1, '', '2019-03-25 21:43:15', 0),
(4, 1, 15, 1, '', '2019-03-24 22:00:30', 0),
(5, 1, 24, 1, '', '2019-03-28 23:53:50', 0),
(6, 1, 32, 1, '', '2019-03-29 17:25:05', 0),
(7, 1, 34, 1, '', '2019-03-28 23:45:48', 0),
(8, 1, 26, 1, '', '2019-03-28 23:45:51', 0),
(9, 1, 16, 1, '', '2019-03-25 22:10:35', 0),
(10, 1, 7, 1, '', '2019-03-25 22:30:38', 0),
(11, 1, 18, 1, '', '2019-03-28 23:03:13', 0),
(12, 1, 5, 1, '', '2019-03-29 17:24:58', 0),
(13, 1, 1, 1, '', '2019-03-29 18:31:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `order_id` varchar(100) NOT NULL,
  `pembeli_id` int(11) NOT NULL,
  `pay_status` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`order_id`, `pembeli_id`, `pay_status`, `created`) VALUES
('ORD120190324220008', 1, 1, '2019-03-24 22:00:08'),
('ORD120190324220035', 1, 1, '2019-03-24 22:00:35'),
('ORD120190325221043', 1, 1, '2019-03-25 22:10:43'),
('ORD120190325223042', 1, 1, '2019-03-25 22:30:42'),
('ORD120190327195547', 1, 1, '2019-03-27 19:55:47'),
('ORD120190329172511', 1, 0, '2019-03-29 17:25:11');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_orderlist`
--

CREATE TABLE `tbl_orderlist` (
  `order_id` varchar(100) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT '1',
  `notes` varchar(200) DEFAULT NULL,
  `total` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_orderlist`
--

INSERT INTO `tbl_orderlist` (`order_id`, `produk_id`, `jumlah`, `notes`, `total`) VALUES
('ORD120190324220008', 33, 1, 'Warna Biru', '99500'),
('ORD120190324220035', 13, 1, '', '5950000'),
('ORD120190324220035', 15, 1, '', '3450000'),
('ORD120190325221043', 16, 1, '', '4269000'),
('ORD120190325223042', 7, 3, '', '222000'),
('ORD120190327195547', 32, 2, '', '190000'),
('ORD120190329172511', 5, 1, '', '11200000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pembeli`
--

CREATE TABLE `tbl_pembeli` (
  `pembeli_id` int(11) NOT NULL,
  `pembeli_nama` varchar(50) NOT NULL,
  `pembeli_email` varchar(30) NOT NULL,
  `pembeli_password` varchar(255) NOT NULL,
  `pembeli_hp` varchar(15) NOT NULL,
  `pembeli_alamat` varchar(100) NOT NULL,
  `pembeli_pict` varchar(255) DEFAULT NULL,
  `pembeli_created` datetime NOT NULL,
  `pembeli_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pembeli`
--

INSERT INTO `tbl_pembeli` (`pembeli_id`, `pembeli_nama`, `pembeli_email`, `pembeli_password`, `pembeli_hp`, `pembeli_alamat`, `pembeli_pict`, `pembeli_created`, `pembeli_updated`) VALUES
(1, 'Dimas Rangga', 'dimas@gmail.com', '$2y$10$FFoRijKM5m3BXOcHDAvUqufM/mL/UA1WhaJCHTWrNGfVPjMGLb3ci', '085230896776', 'Jl. Gajah Mada XXVIII', '12321383_1045783818827114_5949235762872304931_n.jpg', '2019-03-16 14:16:58', '2019-03-16 14:16:58'),
(2, 'Rangga', 'rangga@gmail.com', '$2y$10$9Y13i55Wou2YaYuswuvmZeAyyGdjEy2CAb1iph4CSGUZ1io6dBq8i', '085230896776', 'Jl. mangga', NULL, '2019-03-20 22:07:54', '2019-03-20 22:07:54'),
(3, 'enggar', 'enggar@gmail.com', '$2y$10$WI4cs1azfJ.8DTu/cDEgEuTuJHpRSlTAYNv8mYyq44sEim9sqQ10a', '0987667', 'Jl. Keputih', NULL, '2019-03-21 22:55:56', '2019-03-21 22:55:56'),
(4, 'Ronald Bergman', 'ronald@gmail.com', '$2y$10$.OkPK7LnydGtO//dOiszx.vAfqqkJOgtja2iXQy2yV8ABI/A4rIi2', '085467319', 'San Fransisco', NULL, '2019-03-23 17:35:16', '2019-03-23 17:35:16'),
(5, 'Firman', 'firman@gmail.com', '$2y$10$CvXV2EwVkp6c5Ai4xZNYfeThtbJfcXtNyhVwoodl/XQiEUJxJISFa', '9464645', 'Lumajang', NULL, '2019-03-23 19:21:28', '2019-03-23 19:21:28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_produk`
--

CREATE TABLE `tbl_produk` (
  `produk_id` int(11) NOT NULL,
  `toko_id` int(11) NOT NULL,
  `produk_nama` varchar(255) NOT NULL,
  `produk_harga` varchar(50) NOT NULL,
  `produk_stock` int(11) NOT NULL,
  `produk_pict` varchar(255) DEFAULT NULL,
  `produk_deskripsi` varchar(5000) DEFAULT NULL,
  `produk_created` datetime NOT NULL,
  `produk_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_produk`
--

INSERT INTO `tbl_produk` (`produk_id`, `toko_id`, `produk_nama`, `produk_harga`, `produk_stock`, `produk_pict`, `produk_deskripsi`, `produk_created`, `produk_updated`) VALUES
(1, 1, 'Laptop HP Pavilion x360 14-cd1023tx - i3-8145U/14\"/1TB/MX130 2GB/WIN10', '8.650.000', 8, '4474515_27e9fa7f-9dac-4bb0-a7f5-dc0e26dbe3a2_800_600.png', 'Laptop HP Pavilion x360 14-cd1023tx - i3-8145U/14\"/1TB/MX130 2GB/WIN10\r\n\r\nColor: 14-cd1023TX (Silver)\r\ni3-8145U / 14.0\" HD Touch / 4GB DDR4 / 1TB / NVIDIA MX130 2GB / Win 10 Home / No ODD / 2 Years\r\n\r\nSpecification:\r\nProsesor: Intel® Core™ i3-8145U\r\nOperating system: Windows 10 Home\r\nDisplay: 14\" diagonal HD SVA micro-edge WLED-backlit multitouch-enabled edge-to-edge glass (1366 x 768)\r\nMemory: 4 GB DDR4-2400 SDRAM (1 x 4 GB)\r\nInternal storage: 1 TB 5400 rpm SATA\r\nGraphics: NVIDIA® GeForce® MX130 (2 GB GDDR5 dedicated)\r\nOptical Drive: No ODD\r\nGaransi resmi HP Indonesia 2 Tahun.\r\n\r\nSumber Informasi & Spesifikasi: HP Indonesia.\r\nDas :D\r\n\r\n--\r\nTerima kasih banyak Anda telah mengunjungi toko kami:D\r\nSukses & Sehat selalu untuk Anda:D\r\n\r\nHormat kami,\r\nDas Technology\r\nYour Best Tech Partner & Solution Center - Authorized Master Dealer\r\n- Mall Mangga Dua Lt. 3 No.45 - Visit our Playroom Stores to test products\r\n- Checkout our 5 other stores - Locations @ our IG\r\n- IG: @Das_Tech (Follow for Giveaways, Info & Promo :D)\r\n- Telp & WA: 085945137592 / 0216597574\r\n- E: The.dastechnology@gmail•com\r\n- Web: www•thedastech•com\r\n- Jam Kerja: 10.30-17.30 (Pengiriman hari yang sama < jam 15.30)\r\n- More Products: www.tokopedia.com/daslivia (Favoritkan kami yah :D)\r\n\r\nNB: \r\n1.	Hubungi kami via fitur \"Diskusi Produk\'\' Tokopedia untuk respons tercepat :D \r\n2.	Cantumkan permintaan seperti: Warna, Jenis, Packing, Waktu, Dll di kolom keterangan pembelian. Jika tidak ada permintaan pada invoice, kami yang akan membantu menentukan yah :D\r\n3.	Kami merupakan Master Dealer Resmi, seluruh produk kami dijamin Original, Ready & Siap Dipesan.\r\n\r\n- More Info & FAQs: \r\nwww.tokopedia.com/daslivia/note/513615/kepuasan-anda-adalah-hal-yang-terpenting-bagi-kami-klik-untuk-informasi-d', '2019-03-03 00:00:00', '2019-03-03 00:00:00'),
(2, 1, 'Notebook / Laptop DELL Inspiron 3180 11.6\" AMD A9-9420 - Resmi', '4.200.000', 7, '4474515_e00ce7a9-9d95-4e28-bc55-82b98b708e7b_340_340.jpg', 'Notebook / Laptop DELL Inspiron 3180 11.6\" AMD A9-9420 - Resmi\r\n\r\nProcessor: AMD A9 9420e dengan Core Clock hingga 3,6 Ghz\r\nGrafis: AMD Radeon R5 dengan 3 Compute Unit (192 SP) GPU Clock 800 Mhz\r\nLayar: 11.6” HD (1366×768) Anti-Glare LED-Backlit Display\r\nRam: 4GB DDR4\r\nHardisk: 500GB\r\nWebcam: 720p HD Webcam\r\nI/O: 1x HDMI v1.4a\r\n1x USB 3.1 Gen 1\r\n1x USB 2.0\r\n1x Headphone/Mic\r\n1x Micro SD card reader (SD/SDHC/SDXC)\r\nKoneksi: Combo Wi-fi, 802.11bgn + Bluetooth 4.0, 2.4 GHz\r\nSound: Stereo Speaker dengan Dell AudioMaxx\r\nBatterai + Adapter: 2 Cell 32whr dengan 45W adapter\r\nOperasi Sistem: Windows 10 Home\r\n\r\nSumber Informasi & Spesifikasi: Dell Indonesia.\r\nDas :D\r\n\r\n--\r\nTerima kasih banyak Anda telah mengunjungi toko kami:D\r\nSukses & Sehat selalu untuk Anda:D\r\n\r\nHormat kami,\r\nDas Technology\r\nYour Best Tech Partner & Solution Center - Authorized Master Dealer\r\n- Mall Mangga Dua Lt. 3 No.45 - Visit our Playroom Stores to test products\r\n- Checkout our 5 other stores - Locations @ our IG\r\n- IG: @Das_Tech (Follow for Giveaways, Info & Promo :D)\r\n- Telp & WA: 085945137592 / 0216597574\r\n- E: The.dastechnology@gmail•com\r\n- Web: www•thedastech•com\r\n- Jam Kerja: 10.30-17.30 (Pengiriman hari yang sama < jam 15.30)\r\n- More Products: www.tokopedia.com/daslivia (Favoritkan kami yah :D)\r\n\r\nNB: \r\n1.	Hubungi kami via fitur \"Diskusi Produk\'\' Tokopedia untuk respons tercepat :D \r\n2.	Cantumkan permintaan seperti: Warna, Jenis, Packing, Waktu, Dll di kolom keterangan pembelian. Jika tidak ada permintaan pada invoice, kami yang akan membantu menentukan yah :D\r\n3.	Kami merupakan Master Dealer Resmi, seluruh produk kami dijamin Original, Ready & Siap Dipesan.\r\n\r\n- More Info & FAQs: \r\nwww.tokopedia.com/daslivia/note/513615/kepuasan-anda-adalah-hal-yang-terpenting-bagi-kami-klik-untuk-informasi-d', '2019-03-03 00:00:00', '2019-03-03 00:00:00'),
(4, 1, 'Laptop Lenovo Ideapad IP130-14IKB 81H6001NID BLACK - i3-6006U/4GB/DOS', '5.510.000', 5, '4474515_1c19afe4-ca2b-4963-baa7-7012a7014e9a_800_800.jpg', 'Laptop Lenovo Ideapad IP130-14IKB 81H6001NID BLACK - i3-6006U/4GB/DOS\r\n\r\nSpecification:\r\nIP130-14IKB 81H6001NID (Black)\r\n\r\nCORE i3 DOS , i3 6006U (2.0 Ghz / 3 MB L2 Cache), 4GB DDR4 Onboard + 1 Slot ( Max.8 GB ) , 1 TB, 14.0 HD, INTEL GRAPHICS, DOS, DVD RW , 0.3 Megapixels Camera, 2 Cell 30WH Battery, Black 45 Watt 2-Pin, Wifi 1 x 1 802.11 AC + Bluetooth 4.1, Stereo Speaker, NO BACKLIT KEYBOARD, 2 Yrs Warranty, 338.3 x 249.9 x 22.7 mm, 2.1 kg, 1 x USB Type-C™, 2 x USB 3.0, 4-in-1 Card Reader, HDMI™, RJ45, Audio Jack\r\n\r\nSumber Informasi & Spesifikasi: Lenovo Indonesia.\r\nDas :D\r\n\r\n--\r\nTerima kasih banyak Anda telah mengunjungi toko kami:D\r\nSukses & Sehat selalu untuk Anda:D\r\n\r\nHormat kami,\r\nDas Technology\r\nYour Best Tech Partner & Solution Center - Authorized Master Dealer\r\n- Mall Mangga Dua Lt. 3 No.45 - Visit our Playroom Stores to test products\r\n- Checkout our 5 other stores - Locations @ our IG\r\n- IG: @Das_Tech (Follow for Giveaways, Info & Promo :D)\r\n- Telp & WA: 085945137592 / 0216597574\r\n- E: The.dastechnology@gmail•com\r\n- Web: www•thedastech•com\r\n- Jam Kerja: 10.30-17.30 (Pengiriman hari yang sama < jam 15.30)\r\n- More Products: www.tokopedia.com/daslivia (Favoritkan kami yah :D)\r\n\r\nNB: \r\n1.	Hubungi kami via fitur \"Diskusi Produk\'\' Tokopedia untuk respons tercepat :D \r\n2.	Cantumkan permintaan seperti: Warna, Jenis, Packing, Waktu, Dll di kolom keterangan pembelian. Jika tidak ada permintaan pada invoice, kami yang akan membantu menentukan yah :D\r\n3.	Kami merupakan Master Dealer Resmi, seluruh produk kami dijamin Original, Ready & Siap Dipesan.\r\n\r\n- More Info & FAQs: \r\nwww.tokopedia.com/daslivia/note/513615/kepuasan-anda-adalah-hal-yang-terpenting-bagi-kami-klik-untuk-informasi-d', '2019-03-02 00:00:00', '2019-03-02 00:00:00'),
(5, 1, 'PC HP 280 G4 PCI MT PC (2SJ42AV) - i5-8400/4GB/1TB/18.5\"/Win10Pro', '11.200.000', 5, '4474515_7f2303ba-3307-4152-a313-16ccef6ed079_763_765.png', 'PC HP 280 G4 PCI MT PC (2SJ42AV) - i5-8400/4GB/1TB/18.5\"/Win10Pro\r\n\r\nSpecification:\r\nPart Number: 2SJ42AV\r\nIntel Core i5- 8400 2.8GHz 6C 65W , Win 10 Pro 64, 4GB (1x4GB) DDR4 2666 UDIMM NECC , 1TB 7200 SATA-6G 3.5 , 9.5 DVDWR 280G4/285G3 MT/SFF , Realtek ac1x1 +BT4.2 WW, HP PCIe x1 Parallel Port Card , 3YR INDO Warranty Card , 3/3/3 MT Warranty , HP V194 18.5-IN Monitor\r\n\r\nSumber Informasi & Spesifikasi: HP Indonesia.\r\nDas :D\r\n\r\n--\r\nTerima kasih banyak Anda telah mengunjungi toko kami:D\r\nSukses & Sehat selalu untuk Anda:D\r\n\r\nHormat kami,\r\nDas Technology\r\nYour Best Tech Partner & Solution Center - Authorized Master Dealer\r\n- Mall Mangga Dua Lt. 3 No.45 - Visit our Playroom Stores to test products\r\n- Checkout our 5 other stores - Locations @ our IG\r\n- IG: @Das_Tech (Follow for Giveaways, Info & Promo :D)\r\n- Telp & WA: 085945137592 / 0216597574\r\n- E: The.dastechnology@gmail•com\r\n- Web: www•thedastech•com\r\n- Jam Kerja: 10.30-17.30 (Pengiriman hari yang sama < jam 15.30)\r\n- More Products: www.tokopedia.com/daslivia (Favoritkan kami yah :D)\r\n\r\nNB: \r\n1.	Hubungi kami via fitur \"Diskusi Produk\'\' Tokopedia untuk respons tercepat :D \r\n2.	Cantumkan permintaan seperti: Warna, Jenis, Packing, Waktu, Dll di kolom keterangan pembelian. Jika tidak ada permintaan pada invoice, kami yang akan membantu menentukan yah :D\r\n3.	Kami merupakan Master Dealer Resmi, seluruh produk kami dijamin Original, Ready & Siap Dipesan.\r\n\r\n- More Info & FAQs: \r\nwww.tokopedia.com/daslivia/note/513615/kepuasan-anda-adalah-hal-yang-terpenting-bagi-kami-klik-untuk-informasi-d', '2019-02-28 00:00:00', '2019-02-28 00:00:00'),
(6, 1, 'PC HP AIO 22-c0033d (3JV77AA) - Intel i3-8130U/4GB/1TB/21.5\"FHD/WIN10', '7.350.000', 5, '4474515_69ba04f5-7179-41fd-92c2-e002d37f552c_800_600.png', 'PC HP AIO 22-c0033d (3JV77AA) - Intel i3-8130U/4GB/1TB/21.5\"FHD/WIN10\r\n\r\nSpecification:\r\nPart Number: 3JV77AA\r\nIntel® Core™ i3-8130U Processor \r\nWindows 10 \r\n4 GB DDR4-2400 SDRAM (1 x 4 GB)\r\n1 TB 7200 rpm SATA\r\nIntel UHD Graphics 620\r\n21.5\" diagonal FHD IPS anti-glare WLED-backlit three-sided borderless (1920 x 1080)\r\n\r\nSumber Informasi & Spesifikasi: HP Indonesia.\r\nDas :D\r\n\r\n--\r\nTerima kasih banyak Anda telah mengunjungi toko kami:D\r\nSukses & Sehat selalu untuk Anda:D\r\n\r\nHormat kami,\r\nDas Technology\r\nYour Best Tech Partner & Solution Center - Authorized Master Dealer\r\n- Mall Mangga Dua Lt. 3 No.45 - Visit our Playroom Stores to test products\r\n- Checkout our 5 other stores - Locations @ our IG\r\n- IG: @Das_Tech (Follow for Giveaways, Info & Promo :D)\r\n- Telp & WA: 085945137592 / 0216597574\r\n- E: The.dastechnology@gmail•com\r\n- Web: www•thedastech•com\r\n- Jam Kerja: 10.30-17.30 (Pengiriman hari yang sama < jam 15.30)\r\n- More Products: www.tokopedia.com/daslivia (Favoritkan kami yah :D)\r\n\r\nNB: \r\n1.	Hubungi kami via fitur \"Diskusi Produk\'\' Tokopedia untuk respons tercepat :D \r\n2.	Cantumkan permintaan seperti: Warna, Jenis, Packing, Waktu, Dll di kolom keterangan pembelian. Jika tidak ada permintaan pada invoice, kami yang akan membantu menentukan yah :D\r\n3.	Kami merupakan Master Dealer Resmi, seluruh produk kami dijamin Original, Ready & Siap Dipesan.\r\n\r\n- More Info & FAQs: \r\nwww.tokopedia.com/daslivia/note/513615/kepuasan-anda-adalah-hal-yang-terpenting-bagi-kami-klik-untuk-informasi-d', '2019-03-05 00:00:00', '2019-03-05 00:00:00'),
(7, 1, 'Mouse Gaming NYK G07 / G-07 Turbo Fire with 7 control buttons', '74.000', 24, '16655158_d610faf5-1671-404a-8157-ae8166de0cc1_600_600.jpg', 'Mouse Gaming NYK G07 / G-07 Turbo Fire with 7 control buttons\r\n\r\nReview Das:\r\nMouse Gaming NYK G07 + Lampu LED, adanya 7 tombol dengan tombol doubel klik, sangat cocok untuk gaming dan kebutuhan multimedia Anda. Bahan nyaman, motif keren, & berkualitas dari NYK.\r\n\r\nSpesifikasi:\r\n- Power - related features power source USB\r\n- High speed USB 2.0 PC connectivity\r\n- Gaming mouse with 7 control buttons\r\n- Ergonomic design for professional gamer\r\n- Special gaming bluetrack sensor with\r\nRevolutionary 1600 DPI\r\n- 3 swithching DPI button\r\n- Fashionble style\r\n- Colorfull lights\r\n\r\nSumber Informasi & Spesifikasi: NYK Indonesia.\r\nDas :D\r\n\r\n--\r\nTerima kasih banyak Anda telah mengunjungi toko kami:D\r\nSukses & Sehat selalu untuk Anda:D\r\n\r\nHormat kami,\r\nDas Technology\r\nYour Best Tech Partner & Solution Center - Authorized Master Dealer\r\n- Mall Mangga Dua Lt. 3 No.45 - Visit our Playroom Stores to test products\r\n- Checkout our 5 other stores - Locations @ our IG\r\n- IG: @Das_Tech (Follow for Giveaways, Info & Promo :D)\r\n- Telp & WA: 085945137592 / 0216597574\r\n- E: The.dastechnology@gmail•com\r\n- Web: www•thedastech•com\r\n- Jam Kerja: 10.30-17.30 (Pengiriman hari yang sama < jam 15.30)\r\n- More Products: www.tokopedia.com/daslivia (Favoritkan kami yah :D)\r\n\r\nNB: \r\n1.	Hubungi kami via fitur \"Diskusi Produk\'\' Tokopedia untuk respons tercepat :D \r\n2.	Cantumkan permintaan seperti: Warna, Jenis, Packing, Waktu, Dll di kolom keterangan pembelian. Jika tidak ada permintaan pada invoice, kami yang akan membantu menentukan yah :D\r\n3.	Kami merupakan Master Dealer Resmi, seluruh produk kami dijamin Original, Ready & Siap Dipesan.\r\n\r\n- More Info & FAQs: \r\nwww.tokopedia.com/daslivia/note/513615/kepuasan-anda-adalah-hal-yang-terpenting-bagi-kami-klik-untuk-informasi-d', '2019-02-27 00:00:00', '2019-02-27 00:00:00'),
(8, 1, 'Corsair M65 PRO RGB Gaming Mouse (CH-9300111-AP) - White', '1.004.000', 12, '4474515_19586708-4458-49f4-85b2-8ca68060afbd_762_765.png', 'Corsair M65 PRO RGB Gaming Mouse (CH-9300111-AP) - White\r\n\r\nReview Das:\r\nM65 PRO RGB adalah gaming gaming FPS kelas kompetisi dengan teknologi yang Anda butuhkan untuk menang, kustomisasi untuk menjadikannya milik Anda sendiri, dan membangun kualitas untuk bertahan.\r\n\r\nSpecification:\r\nMouse Warranty: Two years\r\nProg Buttons: 8\r\nDPI: 12,000 DPI\r\nSensor: PMW3360\r\nSensor Type: Optical\r\nMouse Backlighting: 3 Zone RGB\r\nOn Board Memory: Yes\r\nOn-board Memory Profiles: No\r\nMouse button Type: Omron\r\nConnectivity: Wired\r\nMouse Button Durability: 20M L/R Click\r\nGrip Type: Claw\r\nWeight Tuning: Yes\r\n\r\nCUE Software: Supported in CUE 2.0\r\nCable: 1.8m Braided Fiber\r\nGame Type: FPS\r\nMouse Feet: Extra Large PTFE\r\nMouse Model: M65 PRO RGB\r\nReport Rate: Selectable 1000Hz/500Hz/250Hz/125Hz\r\nMouse Dimensions: 118mm x 72mm x 39mm\r\n\r\nSumber Informasi & Spesifikasi: Corsair Indonesia.\r\nDas :D\r\n\r\n--\r\nTerima kasih banyak Anda telah mengunjungi toko kami:D\r\nSukses & Sehat selalu untuk Anda:D\r\n\r\nHormat kami,\r\nDas Technology\r\nYour Best Tech Partner & Solution Center - Authorized Master Dealer\r\n- Mall Mangga Dua Lt. 3 No.45 - Visit our Playroom Stores to test products\r\n- Checkout our 5 other stores - Locations @ our IG\r\n- IG: @Das_Tech (Follow for Giveaways, Info & Promo :D)\r\n- Telp & WA: 085945137592 / 0216597574\r\n- E: The.dastechnology@gmail•com\r\n- Web: www•thedastech•com\r\n- Jam Kerja: 10.30-17.30 (Pengiriman hari yang sama < jam 15.30)\r\n- More Products: www.tokopedia.com/daslivia (Favoritkan kami yah :D)\r\n\r\nNB: \r\n1.	Hubungi kami via fitur \"Diskusi Produk\'\' Tokopedia untuk respons tercepat :D \r\n2.	Cantumkan permintaan seperti: Warna, Jenis, Packing, Waktu, Dll di kolom keterangan pembelian. Jika tidak ada permintaan pada invoice, kami yang akan membantu menentukan yah :D\r\n3.	Kami merupakan Master Dealer Resmi, seluruh produk kami dijamin Original, Ready & Siap Dipesan.\r\n\r\n- More Info & FAQs: \r\nwww.tokopedia.com/daslivia/note/513615/kepuasan-anda-adalah-hal-yang-terpenting-bagi-kami-klik-untuk-informasi-d', '2019-03-09 00:00:00', '2019-03-09 00:00:00'),
(9, 2, 'DELL INSPIRON 15 5570 LOKI i5-8250U-W10 - Perak', '10.500.000', 3, '15080945_25b3d601-9c2f-4ff0-8269-48b63d384017_639_417.jpg', 'Dell Inspiron 15 (5570) / LOKI I5-8250U-W-10 :\r\n- Intel Core i5-8250U Processor (6M Cache, up to 3.40 GHz) \r\n-8GB DDR4 2400Mhz \r\n-2TB 5400 rpm Hard Drive \r\n-15.6\" FHD (1920 x 1080) Anti-Glare LED Backlit Display \r\n-Tray Load DVD Drive (Reads and Writes to DVD/CD) \r\n-AMD Radon 530 with 2GB GDDR5 \r\n-Bluetooth 4.1 \r\n-Dell 1810/1820 Wireless 802.11ac \r\n-Windows 10\r\n-1Yr Premium Support: Onsite Service - Retail\r\n-Stereo speakers professionally tuned with MaxxAudio(R) Pro \r\n-65 Watt AC Adapter \r\n-Integrated Widescreen HD Webcam, dual digital microphones \r\n-3-cell battery (integrated) \r\n-McAfee 30day Trial, Ms Office 30day Trial \r\n\r\n\r\nGARANSI RESMI WARRANTY DELL INDONESIA 1 TAHUN !!!\r\n\r\nFREE BACKPACK !!!', '2019-02-10 00:00:00', '2019-02-10 00:00:00'),
(10, 2, 'LAPTOP DELL INSPIRON 13 (7386) ROGUE i5-8265U', '16.000.000', 3, '15080945_049ca1be-769f-4368-806e-9a4b3d3e9942_504_350.jpg', '8th Generation Intel® Core™ i5-8265U processor (6MB Cache, up to 3.90GHz)\r\n8GB On-Board DDR4 2400Mhz \r\n256GB M.2 PCIe NVMe Solid State Drive\r\n13.3” FHD (1920 x 1080) Truelife Touch Narrow Border IPS Display with Active Pen Support\r\nSD Card (SD, SDHC, SDXC)\r\nIntel® UHD Graphics 620\r\nBluetooth \r\n802.11ac\r\nWindows 10 Home Plus Single Language \r\n1Yr Premium Support: Onsite Service - Retail \r\nStereo speakers professionally tuned with MaxxAudio(R) by Waves\r\n45 Watt AC Adapter\r\nIntegrated Widescreen HD 720P Webcam, with Dual Digital Microphone Array\r\n38 WHr, 3-cell battery (integrated)\r\nUSB 3.1 Type-C™\r\nBacklit Keyboard; \r\nMcAfee® Multi Device Security 15 month subscription\r\nSilver\r\nBackpack', '2019-02-17 00:00:00', '2019-02-17 00:00:00'),
(11, 2, 'LAPTOP GAMING DEWA ACER PREDATOR PT715-51 (TRITON 700)', '42.700.000', 2, '15080945_d8998d90-e75b-4de2-927a-424f4756789f_640_480.jpg', 'Intel® Core™ i7-7700HQ Processor (6M Cache, up to 3.80 GHz) \r\n\"15.6” FHD 1920 x 1080 IPS, G-SNYC\r\n\"\r\n2 * 16 GB Dual-channel DDR4 2133 MHz \r\n2 *256GB GB PCIe NVMe SSD \r\nNVIDIA® GeForce® GTX 1080 with 8 GB of GDDR5 \r\n\"4 x speakers with 2 x subwoofers\r\nDolby Audio\r\nPredator PurifiedVoice\r\nSupport Headphone output with Amplifier (600?) \r\n\"\r\n\"SD™ Card reader\r\nUSB 3.1 x 1, Type-C Gen 2: Thunderbolt™ 3 port\r\nUSB 3.0 x 1 with power-off charging\r\nUSB 3.0 x 3\r\nDisplay Port v1.3  *1 (support G-SYNC)\r\nHDMI x 1 (v2.0, support 4K2K output)\r\nMic-in x 1 / Headphone-out x 1\r\nEthernet (RJ-45) port\r\n\r\n\"\r\n230W \r\n3rd WiFi 2x2 AC+ BT M.2\r\nWindows 10 Home', '2019-03-14 00:00:00', '2019-03-14 00:00:00'),
(12, 3, 'ASUS GAMING FX504GE i7-8750 8GB 1TB+128GB GTX1050 WIN10', '15.099.000', 4, '12159238_9c7737a3-bf7d-4bd2-9fca-7709422a0e43.png', 'CPU : Intel Core i7-8750H 2.2 GHz (9M Cache, up to 4.1GHz, 6 Core)\r\nRAM : 8GB DDR4 \r\nHardisk : 1TB SSHD + 128GB PCIe G3 x2 Ssd\r\nLayar ; Layar 15,6 FHD (1920x1080) 60Hz Anti-Glare Panel with 178o wide-view display\r\nVGA : Nvidia GeForce GTX1050Ti with 4GB GDDR5 VRAM\r\nAudio : Built-in Stereo 3.5 W Speakers And Microphone, ASUS Sonic Studio Support Windows 10 Cortana with Voice\r\nKeyboard : Illuminated chiclet keyboard, specially designed WASD keys\r\nBaterai : 64 Whrs 4 Cells Battery\r\nSistem Operasi : Microsoft Windows 10 (64Bit)\r\nColour : Red Pattern\r\nGaransi : 2 tahun ASUS Indonesia\r\nAvaible on 2 series : GUN METAL and RED PATTERN', '2019-01-22 00:00:00', '2019-01-22 00:00:00'),
(13, 3, 'HP 15 AMD RYZEN 3-2200 4GB 1TB VEGA 3 WIN10 15.6INCH', '5.950.000', 7, '18499467_59bfcbd7-c7dd-4237-b307-221a32f701f4_700_525.png', 'HP Notebook - 15-db009au:\r\n- AMD Ryzen 3 2200U (2.5 GHz base frequency, up to 3.4 GHz burst frequency, 1 MB cache, 2 cores)\r\n- 4 GB DDR4-2400 SDRAM (1 x 4 GB)\r\n- AMD Radeon Vega 3 Graphics (Integrated)\r\n- 1 TB 5400 rpm SATA\r\n- 15.6\" diagonal HD SVA BrightView WLED-backlit (1366 x 768)\r\n- Ports 1 multi-format SD media card reader, 1 HDMI 1.4b; 1 Audio Jack; 1 RJ-45; 1 USB 2.0; 2 USB 3.1 Gen 1 (Data transfer only)\r\n- Windows 10 Home Single Language 64\r\n- Garansi Resmi HP 1 Tahun', '2019-01-12 00:00:00', '2019-01-12 00:00:00'),
(14, 4, 'iPhone X 256GB | NEW SEGEL ORIGINAL GARANSI | Apple iPhone 10 256', '13.700.000', 10, '15524491_9baf9a90-2443-493d-abb3-9d3c0ea561bc.png', 'READY STOCK !! ( BUKAN PO )\r\niPhone X 256GB ( iPhone 10 256GB )\r\nPILIHAN WARNA = Silver, Space Gray\r\n\r\nReady juga Iphone x 64GB : \r\nhttps://www.tokopedia.com/evo-store/iphone-x-64gb-new-segel-original-garansi-apple-iphone-10-64\r\n\r\n**( Pengiriman iPhone X menggunakan PACKING KAYU DAN DIASURANSIKAN FULL ( AMAN )\r\n\r\n- NEW-SEGEL-INTER-ORI 1000%\r\n- Garansi Langsung GANTI BARU JIKA ADA KERUSAKAN DLM 7 HARI !**\r\n- BISA KARTU / 4G Seluruh indonesia , Karna Sama 100% Model dgn Keluaran INDO !\r\n- Barang Jaminan ORIGINAL dan BARU dan SEGEL 1000%\r\n- Unit Apple Bisa DiCek dan Tembus di WEB APPLE\r\n( GARANSI UANG BALIK 1000% JIKA BARANG TERBUKTI REKON/ABAL2 )\r\n\r\n* Mau iPhone APA AJA DI Lapak Kami ADA, CEK AJA LAPAK KAMI YA !\r\n\r\n**GARANSI TOKO / GARANSI SINGAPORE !\r\nFREE KLAIM UNIT BARU JIKA ADA MASALAH !\r\nDengan Syarat :\r\n# Jika Kerusakan Bukan Ulah Pengguna ! Bukan Bongkar Sendiri / Masuk Air / Jatuh / Sebab Lain Yg Mengakibatkan Rusak Produk Tsb\r\n# Unit Masih Dalam Kondisi AWALNYA / NO LECET SEDIKITPUN / Aksesoris dan Kotak Harus Masih Lengkap\r\n# Untuk Dalam Masa Garansi Toko Kami Seminggu , Kami Akan Ganti Unit Baru Jika Ada Masalah ( TANPA BIAYA , DENGAN SYARAT DAN KETENTUAN DIATAS )\r\n# Jika Syarat Diatas tidak dpt dipenuhi, maka garansi toko kami hangus.\r\n# Untuk Garansi Apple , Kami Bisa Bantu Klaim jika ada masalah, dan kami akan tarifkan ongkos transportasi utk klaim tersebut. \r\n# Ongkir Retur Bolak Balik sepenuhnya ditanggung oleh pembeli !\r\n\r\nTRUSTED SELLER - TERPERCAYA !\r\nSUDAH BERJUALAN GADGET ONLINE SEJAK 2012\r\nBisa Cek-Ricek Reputasi Saya Sebelum ORDER :D\r\n\r\nContact :\r\nCalvin Jusbin\r\nWhatsapp = 0852 8339 1111\r\nLINE Official = @evostore ( pakai \"@\" )\r\n( ADD LINE@ KAMI UTK Mendapatkan Broadcast HP TERBARU )\r\n\r\nFOLLOW INSTAGRAM OFFICIAL KAMI \r\ninstagram•com/evo•store\r\n( ID = @evo.store )\r\n\r\nPENTING !! BACA CATATAN TOKOPEDIA KAMI SEBELUM ORDER', '2019-02-06 00:00:00', '2019-02-06 00:00:00'),
(15, 4, 'SAMSUNG GEAR S3 FRONTIER Smartwatch | NEW - 100% ORI - SEGEL', '3.450.000', 11, '3701585_0a187810-5e88-4013-b726-e79b3d55b0a2_1200_1200.jpg', 'SAMSUNG GEAR S3 FRONTIER\r\n\r\nHARGA PROMO !!\r\nTERMURAH ! SEGEL ! BARU ! \r\nGARANSI INTER !\r\nGARANSI TOKO 1 MINGGU , Jika ada kerusakan langsung ganti baru !\r\n\r\nSpesifikasi :\r\n-	Layar 1.3 Super AMOLED\r\nCorning Gorilla Glass SR+\r\n-	IP68 certified\r\n-	Resolusi 360 x 360 Pixel\r\n-	OS Tizen-based wearable platform 2.3.2\r\n-	RAM 768 MB\r\n-	CPU Dual-core 1.0 GHz\r\n-	Memori Internal 4 GB\r\n-	Memori Eksternal :No\r\n-	Sensor ; Accelerometer, gyro\r\nheart rate, barometer\r\n-	Baterai Li-lon 380 mAh\r\nwireless charging\r\n\r\n\r\nKelebihan Samsung Gear S3 frontier\r\n-	Menawarkan layar 1,3 inchi sangat pas buat dipergelangan tangan\r\n-	Tampilan semakin kece dan mewah berkat teknogi Super AMOLED Capasitive.\r\n-	Terdapat sertified IP68 yang dapat bertahan dari air\r\n-	Mengadopsi OS Tizen-based wearable platform 2.3.2\r\n-	Kapasitas RAM terbilang mumpuni dengan kapasitas 768 MB\r\n-	Prosessor yang cukup handal degan Dual core 1,0 GHz\r\n-	Internal memori yang terbilang luas dengan kapasitas 4 GB\r\n-	Baterai berkapasitas 380 mAh yang dilengkapi dengan Wireles Charging\r\n\r\nTRUSTED SELLER - TERPERCAYA !\r\nSUDAH BERJUALAN GADGET ONLINE SEJAK 2012\r\nBisa Cek-Ricek Reputasi Saya Sebelum ORDER :D\r\n\r\nContact :\r\nCalvin Jusbin\r\nWhatsapp = 0852 8339 1111\r\nLINE Official = @evostore ( pakai \"@\" )\r\nINSTAGRAM OFFICIAL = @evo.store\r\n( ADD LINE@ & INSTAGRAM OFFICIAL KAMI UTK Mendapatkan Update HP TERBARU )\r\n\r\n** PENTING !! BACA CATATAN TOKOPEDIA KAMI SEBELUM ORDER ! THANKS', '2019-01-15 00:00:00', '2019-01-15 00:00:00'),
(16, 4, 'Xiaomi Mi Pad 4 Plus 64GB RAM 4GB LTE WIFI Tablet PC | MiPad 4 Plus', '4.269.000', 9, '3701585_7ede3b84-40eb-4429-b5b0-de6d9b12f230_968_504.jpg', '#READY#\r\n= Xiaomi Mi Pad 4 plus LTE WIFI =\r\nInternal : 64 GB\r\nEksternal : -\r\nRam : 4 GB RAM\r\nOS : Android 8.1 (Oreo)\r\nChipset : Qualcomm SDM660 Snapdragon 660\r\nWarna : Black, Rose Gold\r\n\r\nSpec Lengkap :\r\n-	Dimensi Body : 245.6 x 149.1 x 8 mm (9.67 x 5.87 x 0.31 in)\r\n-	Berat : 485 g \r\n-	Tipe Layar : IPS LCD capacitive touchscreen, 16M colors\r\n-	Ukuran Layar : 10.1 inches, 295.8 cm2 (~80.8% screen-to-body ratio)\r\n-	Resolusi : 1200 x 1920 pixels, 16:10 ratio (~224 ppi density)\r\n-	CPU : Octa-core (4x2.2 GHz Kryo 260 ; 4x1.8 GHz Kryo 260)\r\n-	GPU : Adreno 512\r\n-	Sim Card : Nano-SIM\r\n-	Bluetooth : 5.0, A2DP, LE\r\n-	Wifi : Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot\r\n-	USB : Type-C 1.0 reversible connector\r\n-	Kamera Belakang :\r\nResolusi : 13 MP (f/2.0)\r\nFitur : Geo-tagging, touch focus, face detection, panorama, HDR\r\nVideo : 1080p@30fps\r\nKamera Depan : 5 MP (f/2.0)\r\n-	Baterai : Non-removable Li-Po 8620 mAh battery\r\n-	Fitur :\r\nSensor Accelerometer, gyro, proximity, compass\r\nMIUI 10\r\n\r\nPenjelasan Barang Yg Anda Dapat =\r\n- PRODUK ORIGINAL 1000%, SIAP BALIKIN UANG Kalau Abal-Abal/Rekon/KW !\r\n- NEW / SEGEl / BNIB Plastikan\r\n- Garansi Inter 1 Tahun\r\n- Garansi Personal 1 Minggu , Garansi LANGSUNG DIGANTI UNIT BARU Jika Bermasalah !\r\n\r\nDengan Syarat :\r\n# Jika Kerusakan Bukan Ulah Pengguna/ Rusak Mendadak ! Bukan Karna Human Error / Masuk Air / Sebab Lain Yg Mengakibatkan Rusak Produk Tsb\r\n# Garansi Tuker Baru Jika Unit Masih Dalam Kondisi BARU / NO LECET / Aksesoris dan Kotak Harus Masih Lengkap\r\n# Ongkos Kirim Retur bolak balik sepenuhnya ditanggung oleh pembeli !\r\n\r\n\r\nTRUSTED SELLER - TERPERCAYA !\r\nSUDAH BERJUALAN GADGET ONLINE SEJAK 2012\r\nBisa Cek-Ricek Reputasi Saya Sebelum ORDER :D\r\n\r\nContact :\r\nCalvin Jusbin\r\nWhatsapp = 0852 8339 1111\r\nLINE Official = @evostore ( pakai \"@\" )\r\n( ADD LINE@ KAMI UTK Mendapatkan Broadcast HP TERBARU )\r\n\r\nFOLLOW INSTAGRAM OFFICIAL KAMI \r\ninstagramcom/evostore\r\n( ID = @evo.store )\r\n\r\nPENTING !! BACA CATATAN KAMI SEBELUM ORDER !!', '2019-02-13 00:00:00', '2019-02-13 00:00:00'),
(17, 5, 'Samsung Galaxy A8+ 2018 GARANSI RESMI SEIN', '5.250.000', 7, '628764_9f9ea5da-7d17-443f-bd21-a18c91801165_839_727.jpg', 'Network Technology \r\nGSM / HSPA / LTE\r\nLaunch Announced 2017, December\r\nStatus Available. Released 2018, January\r\nBody Dimensions 159.9 x 75.7 x 8.3 mm (6.30 x 2.98 x 0.33 in)\r\nWeight 191 g (6.74 oz)\r\nBuild Front/back glass, aluminum frame\r\nSIM Single SIM (Nano-SIM) or Dual SIM (Nano-SIM, dual stand-by)\r\n- Samsung Pay\r\n- IP68 certified - dust/water proof over 1.5 meter and 30 minutes\r\nDisplay Type Super AMOLED capacitive touchscreen, 16M colors\r\nSize 6.0 inches, 91.4 cm2 (~75.5% screen-to-body ratio)\r\nResolution 1080 x 2220 pixels, 18.5:9 ratio (~411 ppi density)\r\nMultitouch Yes\r\nProtection Corning Gorilla Glass (unspecified version)\r\n- Always-on display\r\nPlatform OS Android 7.1.1 (Nougat)\r\nChipset Exynos 7885 Octa\r\nCPU Octa-core (2x2.2 GHz Cortex-A73 & 6x1.6 GHz Cortex-A53)\r\nGPU Mali-G71\r\nMemory Card slot microSD, up to 256 GB (dedicated slot)\r\nInternal 64 GB, 6 GB RAM\r\nCamera Primary 16 MP, f/1.7, phase detection autofocus, LED flash\r\nFeatures Geo-tagging, touch focus, face detection, panorama, HDR\r\nVideo 1080p@30fps\r\nSecondary Dual: 16 MP + 8 MP, f/1.9, 1080p\r\nSound Alert types Vibration; MP3, WAV ringtones\r\nLoudspeaker Yes\r\n3.5mm jack Yes\r\n- Active noise cancellation with dedicated mic\r\nComms WLAN Wi-Fi 802.11 a/b/g/n/ac, dual-band, WiFi Direct, hotspot\r\nBluetooth 5.0, A2DP, EDR, LE\r\nGPS Yes, with A-GPS, GLONASS, BDS\r\nNFC Yes\r\nRadio FM radio\r\nUSB 2.0, Type-C 1.0 reversible connector\r\nFeatures Sensors Fingerprint (rear-mounted), accelerometer, gyro, proximity, compass, barometer\r\nMessaging SMS(threaded view), MMS, Email, Push Mail, IM\r\nBrowser HTML5\r\n- Fast battery charging\r\n- ANT+ support\r\n- MP4/WMV/H.265 player\r\n- MP3/WAV/WMA/eAAC+/FLAC player\r\n- Photo/video editor\r\n- Document viewer\r\nBattery Non-removable Li-Ion 3500 mAh battery', '2019-03-08 00:00:00', '2019-03-08 00:00:00'),
(18, 5, 'Nokia 2 2018 GARANSI RESMI NOKIA', '860.000', 16, '628764_0c311d63-3d13-4bcb-ba7d-d11ff31a8f0b_839_727.jpg', 'Network Technology\r\nGSM / HSPA / LTE\r\nBody Dimensions 143.5 x 71.3 x 9.3 mm (5.65 x 2.81 x 0.37 in)\r\nWeight 161 g (5.68 oz)\r\nBuild Aluminum frame, plastic back\r\nSIM Single SIM (Nano-SIM) or Dual SIM (Nano-SIM, dual stand-by)\r\nDisplay Type LTPS IPS LCD capacitive touchscreen, 16M colors\r\nSize 5.0 inches, 68.9 cm2 (~67.4% screen-to-body ratio)\r\nResolution 720 x 1280 pixels, 16:9 ratio (~294 ppi density)\r\nMultitouch Yes\r\nProtection Corning Gorilla Glass 3\r\nPlatform OS Android 7.1.1 (Nougat), planned upgrade to Android 8.1 (Oreo)\r\nChipset Qualcomm MSM8909v2 Snapdragon 212\r\nCPU Quad-core 1.3 GHz Cortex-A7\r\nGPU Adreno 304\r\nMemory Card slot microSD, up to 128 GB (dedicated slot)\r\nInternal 8 GB, 1 GB RAM\r\nCamera Primary 8 MP, autofocus, LED flash, check quality\r\nFeatures Geo-tagging, touch focus\r\nVideo 720p@30fps\r\nSecondary 5 MP\r\nSound Alert types Vibration; MP3, WAV ringtones\r\nLoudspeaker Yes\r\n3.5mm jack Yes\r\nComms WLAN Wi-Fi 802.11 b/g/n, hotspot\r\nBluetooth 4.1, A2DP, LE\r\nGPS Yes, with A-GPS, GLONASS, BDS\r\nRadio FM radio with RDS\r\nUSB microUSB 2.0\r\nFeatures Sensors Accelerometer, proximity, compass\r\nMessaging SMS(threaded view), MMS, Email, Push Mail, IM\r\nBrowser HTML5\r\n- MP4/H.264 player\r\n- MP3/WAV/eAAC+/FLAC player\r\n- Photo/video editor\r\n- Document viewer\r\nBattery Non-removable Li-Ion 4100 mAh battery', '2018-12-13 00:00:00', '2018-12-13 00:00:00'),
(19, 5, 'Xiaomi Mi 8 Llite 4/64 RAM 4GB ROM 64GB GARANSI RESMI TAM', '3.530.000', 8, '628764_0c311d63-3d13-4bcb-ba7d-d11ff31a8f0b_839_727.jpg', 'NETWORK	Technology	\r\nGSM / CDMA / HSPA / LTE\r\nLAUNCH	Announced	2018, September\r\nStatus	Available. Released 2018, September\r\nBODY	Dimensions	156.4 x 75.8 x 7.5 mm (6.16 x 2.98 x 0.30 in)\r\nWeight	169 g (5.96 oz)\r\nBuild	Front/back glass, aluminum frame\r\nSIM	Hybrid Dual SIM (Nano-SIM, dual stand-by)\r\nDISPLAY	Type	IPS LCD capacitive touchscreen, 16M colors\r\nSize	6.26 inches, 97.8 cm2 (~82.5% screen-to-body ratio)\r\nResolution	1080 x 2280 pixels, 19:9 ratio (~403 ppi density)\r\nMultitouch	Yes\r\n- MIUI\r\nPLATFORM	OS	Android 8.1 (Oreo)\r\nChipset	Qualcomm SDM660 Snapdragon 660 (14 nm)\r\nCPU	Octa-core (4x2.2 GHz Kryo 260 & 4x1.8 GHz Kryo 260)\r\nGPU	Adreno 512\r\nMEMORY	Card slot	microSD, up to 256 GB (uses SIM 2 slot)\r\nInternal	64 GB, 4 GB RAM\r\nMAIN CAMERA	Dual	12 MP, f/1.9, 1/2.55\", 1.4µm, dual pixel PDAF\r\n5 MP, f/2.0, 1/5\", 1.12µm, depth sensor\r\nFeatures	LED flash, HDR, panorama\r\nVideo	2160p@30fps, 1080p@60/120fps, 1080p@30fps (gyro-EIS)\r\nSELFIE CAMERA	Single	24 MP, 1/2.8\", 0.9µm\r\nFeatures	Auto HDR\r\nVideo	1080p@30fps\r\nSOUND	Alert types	Vibration; MP3, WAV ringtones\r\nLoudspeaker	Yes\r\n3.5mm jack	No\r\n- Active noise cancellation with dedicated mic\r\nCOMMS	WLAN	Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot\r\nBluetooth	5.0, A2DP, LE\r\nGPS	Yes, with A-GPS, GLONASS, BDS\r\nRadio	No\r\nUSB	Type-C 1.0 reversible connector\r\nFEATURES	Sensors	Fingerprint (rear-mounted), accelerometer, gyro, proximity, compass\r\nMessaging	SMS(threaded view), MMS, Email, Push Email, IM\r\nBrowser	HTML5\r\n- Fast battery charging (Quick Charge 3.0)\r\n- MP4/DivX/XviD/WMV/H.265 player\r\n- MP3/WAV/eAAC+/FLAC player\r\n- Photo/video editor\r\n- Document viewer\r\nBATTERY	Non-removable Li-Po 3350 mAh battery', '2019-03-04 00:00:00', '2019-03-04 00:00:00'),
(20, 6, 'ADIDAS QUESTAR FLOW GREEN - Hijau Tua, 39', '330.000', 14, '3533626_4a9f590f-1058-4263-9aa2-e8bbf24ce932_1006_1006.jpg', 'Original made Indonesia\r\nBNWB ( Brand New With repleace box\r\n\r\nNote : - Mohon Konfirmasi dahulu untuk ketersediaan Stocknya\r\n___________________________________________WA.085959902082________________\r\n\r\nMengapa belanja di toko NEVSHOES_ORIGINAL?\r\n- Pelayanan diutamakan :)\r\n- Trusted Seller ( Tergantung penilaian masing\" ) hehehe\r\n- Fast respons ( Selain waktu kami istirahat ) \r\n- Fast delivery ( Cek ulasan ) :)\r\n- Same day service ( Transaksi Sebelum jam 5 Sore akan dikirim dihari itu juga , Gosend atau grab sebelum jam 4 sore ) - Semua yang dijual dijamin ORIGINAL\r\n- Harga sangat kompetitif - Gambar Real Pict, Yang dipajang Tentu yang akan anda dapatkan - Sebelum barang dikirim Selalu dilakukan pengecekan semaksimal mungkin - Packing rapih - Garansi tukar size bila Kurang pas - Proses return barang sangat mudah \r\nDan pasti ** MENJAMIN UANG KEMBALI JIKA BARANG TIDAK ORIGINAL\r\n- Sebelum melakukan transaksi pastikan Calon pembeli sudah membaca dan memahami seluruh catatan toko kami terutama TENTANG PRODUK.\r\n- Dengan anda bertransaksi, kami anggap Pembeli sudah memahami dan mengerti tentang barang maupun kebijakan yg berlaku ditoko kami.', '2019-02-02 00:00:00', '2019-02-02 00:00:00'),
(21, 6, 'ADIDAS GAZELLE NAVY - Navy, 40', '400.000', 13, '3533626_8ac5c579-14c5-4939-be01-4029326aae7b_619_619.jpg', 'Original made Indonesia\r\nBNWB ( Brand New With repleace Box )\r\n\r\nAvailable Size \r\nEuro :\r\n\r\n- 36 \r\n- 36 2/3\r\n- 37 1/3\r\n- 38\r\n- 38 2/3 \r\n- 39 1/3 \r\n- 40 \r\n- 40 2/3 \r\n\r\nNote : - Mohon Konfirmasi dahulu untuk ketersediaan Stocknya\r\nWA.085959902082\r\n\r\nMengapa belanja di toko NEVSHOES_ORIGINAL?\r\n- Pelayanan diutamakan :)\r\n- Trusted Seller ( Tergantung penilaian masing\" ) hehehe\r\n- Fast respons ( Selain waktu kami istirahat ) \r\n- Fast delivery ( Cek ulasan ) :)\r\n- Same day service ( Transaksi Sebelum jam 5 Sore akan dikirim dihari itu juga , Gosend atau grab sebelum jam 4 sore ) - Semua yang dijual dijamin ORIGINAL\r\n- Harga sangat kompetitif - Gambar Real Pict, Yang dipajang Tentu yang akan anda dapatkan - Sebelum barang dikirim Selalu dilakukan pengecekan semaksimal mungkin - Packing rapih - Garansi tukar size bila Kurang pas - Proses return barang sangat mudah \r\nDan pasti ** MENJAMIN UANG KEMBALI JIKA BARANG TIDAK ORIGINAL\r\n- Sebelum melakukan transaksi pastikan Calon pembeli sudah membaca dan memahami seluruh catatan toko kami terutama TENTANG PRODUK.\r\n- Dengan anda bertransaksi, kami anggap Pembeli sudah memahami dan mengerti tentang barang maupun kebijakan yg berlaku ditoko kami.', '2019-01-16 00:00:00', '2019-01-16 00:00:00'),
(22, 7, 'sepatu nike rosherun sneakers pria sepatu running', '145.000', 17, '1489596_164fa661-ed16-4715-abc1-5b8df9ea1fea_320_320.jpg', 'Foto sepatu Real Picture minggunakan kamera DSLR..\r\nSudah termasuk DUS/BOX.\r\nSIZE DARI 39,40,41,42,43,44,45\r\nUkuran : 39 panjang alas: 24,5 cm.\r\nUkuran : 40 panjang alas: 25,5 cm.\r\nUkuran : 41 panjang alas: 26,5 cm.\r\nUkuran : 42 Panjang alas: 27,5 cm.\r\nUkuran : 43 Panjang alas: 28,5 cm\r\nUkuran : 44 Panjang alas : 29 cm\r\n100% Barang Baru...\r\nNyaman dipakai untuk aktifitas sehari-hari.\r\n\r\nTanyakan stock sebelum memesan, supaya menghindari refund karna stock sewaktu-waktu habis.\r\n\r\nPastikan mencantumkan warna dan ukuran saat membeli atau cantumkan nomor sesuai gambar, supaya mempercepat proses pengiriman barang.\r\nContoh cara menulis pesanan di diskripsi pemesanan atau di inbox ( Pesan No.3 size 41 )\r\n\r\nCara memesan cukup mudah, Klik Beli dan ikuti petunjuk selanjutnya dari tokopedia', '2019-01-16 00:00:00', '2019-01-16 00:00:00'),
(23, 7, 'New Balance 574 Man Premium Original / sepatu pria', '525.000', 11, '155166419529366_24eeddfd-0bfd-4e9c-ad52-4b6ab4c14f87.png', 'termasuk DUS/BOX. BOX \r\n\r\nUkuran 39-44\r\nNyaman dipakai untuk segala jenis Olahraga, jalan-jalan, trevelling dan melakukan berbagai macam aktifitas sehari-hari.\r\n\r\nTanyakan stock sebelum memesan, supaya menghindari refund karna stock sewaktu-waktu habis.\r\n\r\nPastikan mencantumkan warna dan ukuran saat membeli atau cantumkan nomor sesuai gambar,supaya mempercepat prodses pengiriman barang.\r\n\r\nCara memesan cukup mudah, Klik Beli dan ikuti petunjuk selanjutnya dari Tokopedia', '2018-12-19 00:00:00', '2018-12-19 00:00:00'),
(24, 8, 'BAJU KEMEJA PANJANG KERJA KANTOR FLANEL FLANNEL KOTAK PLANEL BIRU - Seperti di foto, M', '95.000', 24, '259660_c143dd13-423d-4d25-851a-4805f491cab0_2048_2048.jpeg', 'bahan : FLANEL SEMI WOOL high quality & dijamin tidak kecewa..\r\n\r\nSIZE KEBESARAN / KEKECILAN = TUKAR SIZE\r\ndengan syarat :\r\n- laporan paling lama 2 hari setelah barang sampai .\r\n- buyer / customer tanggung ongkir balik pengiriman .\r\n- baju masih dalam kondisi bagus ok seperti saat barang datang .\r\n- label merek baju tidak di copot .\r\n\r\nsize : lokal indonesia cowok normal M, L, XL\r\n\r\nEstimasi ukuran : \r\n- M ( 50 lebar x 70 panjang )\r\n- L ( 52 lebar x 72 panjang )\r\n- XL ( 54 lebar x 74 panjang )\r\n\r\nREADY STOCK ( no po )\r\n\r\nuntuk info lebih tentang produk , reseler , grosir , atau dll. cp:\r\nLINE : @vanshirtstore , pakai \"@\" ya..\r\n( order harus tetap via Tokopedia yang aman & terpecaya )\r\n\r\nnote :\r\n- Silahkan tanya dulu model dan size yang anda inginkan sebelum order ,untuk menghindari kekosongan stock .\r\n- Size , warna , dan bentuk di gambar sesuai 100% dengan keadaan asli .\r\n- Bahan Halus & tidak kasar.', '2018-11-27 00:00:00', '2018-11-27 00:00:00'),
(25, 8, 'JAKET COWOK PRIA POLOS SWEATER HOODIE ZIPPER RESLETING BIRU TUTON - sesuai di foto, L', '65.000', 18, '259660_868f9f91-5136-4e51-8a7e-9398b0182bd6_2048_2048.jpeg', '--- MENERIMA TUKAR SIZE ---\r\nSIZE KEBESARAN / KEKECILAN = TUKAR SIZE\r\ndengan syarat :\r\n- laporan paling lama 2 hari setelah barang sampai .\r\n- buyer / customer tanggung ongkir balik pengiriman .\r\n\r\nsize : lokal indonesia cowok normal M , L , XL\r\n\r\nEstimasi ukuran (TOLERANSI PERBEDAAN 1-2 CM ya) :\r\n( Lebar x Panjang x Panjang Lengan )\r\nL = 52cm x 67cm x 58cm\r\nXL = 54cm x 70cm x 60cm\r\n\r\nREADY STOCK ( no po )\r\n\r\nnote :\r\n- Silahkan tanya dulu model dan size yang anda inginkan sebelum order ,untuk menghindari kekosongan stock .\r\n- Size , warna , dan bentuk di gambar sesuai 100% dengan keadaan asli .\r\n- Bahan Halus & tidak kasar.\r\n\r\nuntuk info lebih tentang produk , reseler , grosir , atau dll. cp:\r\nLINE : @vanshirtstore , pakai \"@\" ya..\r\n( order harus tetap via Tokopedia yang aman & terpecaya )', '2018-12-24 00:00:00', '2018-12-24 00:00:00'),
(26, 8, 'BAJU KEMEJA MOTIF TARTAN KOTAK HITAM FASHION COWOK PRIA - sesuai di foto, XL', '85.000', 24, '259660_72182912-157b-444b-8394-329dad5881f7_2048_2048.jpeg', '--- MENERIMA TUKAR SIZE ---\r\nSIZE KEBESARAN / KEKECILAN = TUKAR SIZE\r\ndengan syarat :\r\n- laporan paling lama 2 hari setelah barang sampai.\r\n- buyer / customer tanggung ongkir balik pengiriman.\r\n- baju masih dalam kondisi bagus ok seperti saat barang datang.\r\n- label merek baju tidak di copot.\r\n\r\nbahan : FLANNEL\r\n\r\nEstimasi ukuran :\r\n- M ( 50 lebar x 70 panjang )\r\n- L ( 52 lebar x 72 panjang )\r\n- XL ( 54 lebar x 74 panjang )\r\n\r\nnote :\r\n- Silahkan tanya dulu model dan size yang anda inginkan sebelum order ,untuk menghindari kekosongan stock .\r\n- Size , warna , dan bentuk di gambar sesuai 100% dengan keadaan asli .\r\n- Bahan Halus & tidak kasar.\r\n\r\nuntuk info lebih tentang produk , reseler ,atau dll. cp:\r\nLINE : @vanshirtstore , pakai \"@\" ya..', '2018-11-27 00:00:00', '2018-11-27 00:00:00'),
(27, 8, 'BAJU KAOS COWOK PRIA SURFING DISTRO RIPCURL RIP CURL 160 - sesuai di foto, M', '57.500', 11, '259660_a3a1928f-18ef-47bb-875f-9d5710a6dbef_2048_2048.jpeg', '* kualitas PREMIUM\r\n* ada Hangtags Label Brand \r\n\r\nbahan : COMBED 30S\r\n\r\nEstimasi ukuran : (toleransi perbedaan size 1-2cm)\r\n- M , 50 lebar x 70 panjang\r\n- L , 52 lebar x 72 panjang\r\n\r\nREADY STOCK ( no po )\r\n\r\nSIZE KEBESARAN / KEKECILAN = TUKAR SIZE\r\ndengan syarat :\r\n- laporan paling lama 2 hari setelah barang sampai .\r\n- buyer / customer tanggung ongkir balik pengiriman .\r\n- baju masih dalam kondisi bagus ok seperti saat barang datang .\r\n- label merek baju tidak di copot .\r\n\r\nnote :\r\n- Silahkan tanya dulu model dan size yang anda inginkan sebelum order ,untuk menghindari kekosongan stock .\r\n- Size , warna , dan bentuk di gambar sesuai 100% dengan keadaan asli .\r\n- Bahan Halus & tidak kasar.\r\n\r\nuntuk info lebih tentang produk , reseler ,atau dll. cp:\r\nLINE : @vanshirtstore , pakai \"@\" ya..\r\n( order harus tetap via Tokopedia yang aman & terpecaya )', '2018-12-24 00:00:00', '2018-12-24 00:00:00'),
(28, 9, 'Kemeja flanel pria / baju hem flannel untuk cowok tangan panjang', '140.000', 17, '311691_00cbb0ef-c3d7-4dc8-b38b-732dbce6f844_1161_1203.jpg', '- Bahan Flannel\r\n- Saat order mohon di info Code barang(ada di setiap foto)-Size yang agan/sis inginkan (contoh flev20-M)\r\n- Transaksi di bawah pkl 16.00wib di kirim hari yang sama (senin-sabtu) di atas itu akan di kirim keesokan hari nya\r\n- Size Chart Panjang x Lebar (ketiak kiri-kanan)\r\nM : 70 x 50 cm\r\nL : 72 x 52 cm\r\nXL : 74 x 54 cm', '2019-03-06 00:00:00', '2019-03-06 00:00:00'),
(29, 9, 'Kemeja Batik Modern Pria Lengan Pendek Slim fit Kerja Cowok 32', '130.000', 14, '311691_b0e9e80a-21b5-4647-8410-67db35f55824_1161_1203.jpg', 'Kategori produk baru dari toko kami , Kemeja Batik kualitas Premium dan Elegant dengan jahitan berkelas Dept Store , untuk melengkapi kebutuhan Anda dalam Dunia Kerja , Acara Formal , dan Cocok juga untuk outfit casual \r\n\r\n- Bahan : Cotton Premium\r\n- Model : Slimfit\r\n- Mohon Sertakan size saat order di catatan pemesanan \r\n- Size Chart :\r\nPanjang x Lebar\r\n\r\nS : 70 x 50 cm\r\nM : 72 x 52 cm\r\nL : 74 x 54 cm\r\nXL : 76 x 56 cm \r\n\r\n** Transaksi di bawah pkl 16.00wib di kirim hari yang sama (senin-sabtu,kecuali hari libur nasional) di atas itu akan di kirim keesokan hari nya', '2018-12-16 00:00:00', '2018-12-16 00:00:00'),
(30, 9, 'Kemeja Oxford Pria Polos Lengan Pendek Cowok Slimfit Merah Maroon ox21', '135.000', 10, '311691_8c6969f8-4c99-46ef-8c3b-af095ea9388a_1161_1203.jpg', '- Model : Slimfit\r\n- Bahan : Oxford\r\n- Mohon Informasikan Size yang di inginkan pada catatan pembelian\r\n- Mohon luangkan waktu untuk bertanya ketersediaan stock dengan diskusi / kirim pesan sebelum order\r\n- Size Chart Panjang x Lebar Dada\r\nM : 72 x 52 cm\r\nL : 74 x 54 cm\r\nXL : 76 x 56 cm\r\n\r\n** Transaksi di bawah pkl 16.00wib di kirim hari yang sama (senin-sabtu,kecuali hari libur nasional) di atas itu akan di kirim keesokan hari nya', '2019-01-29 00:00:00', '2019-01-29 00:00:00'),
(31, 9, 'Baju kaos distro salur polos kaus oblong pria belang navy tm34', '50.000', 16, '311691_e14f57ad-cd23-452f-944e-921e203e2abe_969_1000.jpg', '- Bahan cotton \r\n- Sebelum order tanyakan Stock barang terlebih dahulu untuk menghindari kekosongan stock\r\n- All size / 1 size , Fit M to L \r\n- Size Chart Panjang x Lebar dada\r\n50cm x 70cm\r\n\r\n** Transaksi di bawah pkl 16.00wib di kirim hari yang sama (senin-sabtu,kecuali hari libur nasional) di atas itu akan di kirim keesokan hari nya', '2019-03-09 00:00:00', '2019-03-09 00:00:00'),
(32, 10, 'Celana chino panjang pria murah - Hitam, 28', '95.000', 14, '3351325_4da88442-4d27-40f4-999f-95ae824bf6bc_2048_2048.jpg', 'Spesifikasi Utama :\r\n\r\n- Nyaman dipakai\r\n- Simple & fashionable\r\n- Bahan berkualitas cotton stretch / melar, Tebal, Lembut, Tidak berbulu dan kuat.\r\ncocok digunakan di berbagai acara formal/non formal\r\n\r\nPilihan Warna :\r\n- hitam ( Best Seller )\r\n- abu \r\n- Light Grey\r\n- cream\r\n- coffe \r\n- biru dongker \r\n-Blue\r\n- khakis\r\n-mocca\r\n-stone\r\n- hijau army (XL)\r\n- Putih\r\n- kuning kunyit \r\n- khakis muda\r\n\r\n\r\ntersedia size :\r\nS = 27-28\r\nM= 29-30\r\nL=31-32\r\nXL= 33-34\r\n\r\n(Notice Untuk warna putih ,kuning kunyit dan khakis muda tidak tersedia di pilihan variant Cantumkan saja di deskripsi product dan size yang di inginkan )\r\n\r\nTersedia juga ukuran 35-40 \r\nready warna hitam cream khakis dan mocca \r\nordernya di link dibawah ini \r\nhttps://www.tokopedia.com/oxoshop/celana-chino-panjang-abu-big-size\r\n\r\nTersedia juga untuk size 40-42\r\nready warna hitam dan abu \r\nordernya di link dibawah ini\r\nhttps://www.tokopedia.com/oxoshop/celana-chino-abu-super-big-size\r\n\r\n\r\nNote : Pemilihan Merk tergantung stock yg ada\r\n\r\n\r\nJangan lupa cantumkan warna dan size yg di inginkan di keterangan ordernya ya untuk mempercepat proses pengiriman\r\n\r\n\r\nHappy Shooping\r\nOpen Reseller, Dropshiper, Grosir, Ecer', '2018-12-11 00:00:00', '2018-12-11 00:00:00'),
(33, 10, 'Celana Jeans Slimfit Panjang Pria Biowash scrup or dark grey wash - Biowash Scrup', '99.500', 8, '431910_6bfe0f15-dd46-4bb6-a46b-50904e322734_1024_1024.jpg', 'Celana Jeans Pria keren\r\n\r\nMaterial : Jeans Stretch (bahan ngaret tidak berbulu )\r\nMode : Slimfit\r\nWarna : Dark grey wash & Biowash Scrup\r\nTersedia size : \r\n\r\n? S 27-28\r\n? M 29-30\r\n? L 31-32\r\n? XL 33-34\r\n\r\n\r\nNOTICE : cantumkan size dan warna saat melakukan transaksi nanti\r\n\r\n\r\nHappy Shopping', '2019-02-23 00:00:00', '2019-02-23 00:00:00'),
(34, 10, 'Celana Jeans Pendek Washing Keren', '90.000', 14, '153976063509964_3b1a240f-1a84-416f-8c66-14d9c14799a0.png', 'Celana Jeans Pendek Washing Keren \r\n\r\n\r\nDeskripsi Product\r\n\r\n- Material Jeans Stretch Tidak Mudah Luntur dan tidak berbulu\r\n- Tersedia Size 28-34\r\n- Model Slimfit \r\n- Variant Warna \r\n+ Black Wash\r\n+ Bioblit Scrup\r\n+ Biowash Scrup\r\n\r\nStock Terbatas \r\n\r\nDapatkan Sebelum Kehabiaan', '2018-10-17 00:00:00', '2018-10-17 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_toko`
--

CREATE TABLE `tbl_toko` (
  `toko_id` int(11) NOT NULL,
  `toko_nama` varchar(50) NOT NULL,
  `toko_lokasi` varchar(300) NOT NULL,
  `toko_deskripsi` varchar(1000) DEFAULT NULL,
  `toko_reputasi` varchar(10) DEFAULT NULL,
  `toko_pict` varchar(100) DEFAULT NULL,
  `toko_created` datetime NOT NULL,
  `toko_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_toko`
--

INSERT INTO `tbl_toko` (`toko_id`, `toko_nama`, `toko_lokasi`, `toko_deskripsi`, `toko_reputasi`, `toko_pict`, `toko_created`, `toko_updated`) VALUES
(1, 'Das Livia Computer', 'Mangga Dua Mall Lt 3 No 45, Jalan Mangga Dua Raya. Sawah Besar, Kota Administrasi Jakarta Pusat, 10', 'Das menyediakan solusi kebutuhan Computer & Gadget Anda :D Das percaya akan kepuasan Anda & Kami akan terus berusaha untuk menyediakannya', '5', '1253239_326c6518-fb55-450a-90c7-ad7d46cec508.png', '2018-11-14 00:00:00', '2018-11-14 00:00:00'),
(2, 'Tokolaptopid', 'Sunter, Jakarta Utara, Kota Administrasi Jakarta Utara, 14350', 'RESELLER LANGSUNG & RESMI LAPTOP SEPERTI MSI,DELL,ACER,TOSHIBA,DLL. COD UNTUK WILAYAH JAKARTA', '3', '1721354_65e6dab9-1d43-428b-be9b-f5872532b7e5.png', '2019-01-21 00:00:00', '2019-01-21 00:00:00'),
(3, 'Vegas-Auto ', 'Kota Administrasi Jakarta Utara', 'welcome spesial it & car. thanks yang sudah berbelanja di kami .jaminan barang sesuai pesanan . thanks\r\ncp : 08998199566', '3.5', '480410_60b0aa28-16f6-49d5-a104-1e4b73de4293.jpg', '2018-10-21 00:00:00', '2018-10-21 00:00:00'),
(4, 'EvoStore', 'Lubuk Baja, Batam', 'Add IG KAMI dan LINE@ Kami utk StayUpdate! \r\nChat VIA DISKUSI Produk/INBOX/Bisa Call/WA = 0852 8339 1111', '4', '469247_992b6c69-03c7-40e6-8557-d611ab3b0c7b.jpeg', '2018-07-19 00:00:00', '2018-07-19 00:00:00'),
(5, 'Cellular Mas', 'ITC Cempaka Mas Lantai 4 Blok F no 343\r\nJalan letjen Suprapto\r\nCempaka Putih, Kota Administrasi Jakarta Pusat, 10640', 'WASPADA PENIPUAN!! CELLULAR MAS HANYA 1, TRANSAKSI HANYA DI TOKOPEDIA, HARAP TIDAK MELAKUKAN TRANSAKSI DI LUAR TOKOPEDIA.. TERIMA KASIH!!!', '4.5', '131646_42dc868b-6047-409a-8110-f738c82a60f5.jpg', '2018-02-20 00:00:00', '2018-02-20 00:00:00'),
(6, 'nevshoes', 'Kp.bojong beunying rt.10/rw.05 bojong cikupa tanggerang banten\r\nkp.bojong beunying rt.10.Rw.05(rumah bapa cecep) bojong cikupa tanggerang banten\r\nCikupa, Kab. Tangerang, 15710', 'Kami menjual produk 100% ORIGINAL (BNWB)\r\nOPEN RESELLER DROPSIP BISA KONTEK LANGSUNG NO WA ADMIN .085959902082', '3', '3533626_8bd9587a-578e-446b-bf51-dda10dda4223.jpg', '2018-10-12 00:00:00', '2018-10-12 00:00:00'),
(7, 'Claressa cloth', 'cengkareng jak-bar\r\nTo: tio\r\n Jl. angsana raya rt.007/012 no.37 sampig SMA 96 ( warung es nyoklat ) cengkareng jak-bar\r\n Tlp.085218282530\r\nCengkareng, Kota Administrasi Jakarta Barat, 11720', 'menjual sepatu berkualitas dgn hrga miring', '3.8', '225909_2d7c2a6e-1bd5-4e1e-a302-5ac58be0213d.jpeg', '2018-05-15 00:00:00', '2018-05-15 00:00:00'),
(8, 'VAN SHIRT STORE', 'Penjaringan, Jakarta Utara', 'info cepat, add LINE: @vanshirtstore , pakai \"@\" ya.. , add WA: 08-999-300-776...SILAHKAN DIBACA PERATURAN TOKO SEBELUM ORDER. thx', '4.2', '88822_9bf6643b-02ec-4688-b5b1-791296a5618d.jpeg', '2018-04-18 00:00:00', '2018-04-18 00:00:00'),
(9, 'Planet Jeans', 'Kota Administrasi Jakarta Utara', '- Jam kerja senin-sabtu 09.00-19.00 (kec hari libur / tanggal merah)\r\n- Diluar jam kerja kami slow respond Have a good day !', '4', '95267_0e8d2937-bf2b-4936-80d1-8cdde01734cd.jpg', '2018-04-11 00:00:00', '2018-04-11 00:00:00'),
(10, 'OXO Shop', 'OXO Shop\r\njl rajawali timur gg.kebon jukut 4 no 62/26\r\nAndir, Kota Bandung, 40182', 'Instagram : oxo_shop\r\nLine : oxoshop Whatsapp : 6281321042278 BBM : D6C6AF34 Open Reseller dan Dropshiper', '4.7', '', '2018-01-19 00:00:00', '2018-01-19 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_wishlist`
--

CREATE TABLE `tbl_wishlist` (
  `pembeli_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_wishlist`
--

INSERT INTO `tbl_wishlist` (`pembeli_id`, `produk_id`, `created`) VALUES
(1, 32, '2019-03-29 18:03:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `pembeli_id` (`pembeli_id`),
  ADD KEY `produk_id` (`produk_id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `pembeli_id` (`pembeli_id`);

--
-- Indexes for table `tbl_orderlist`
--
ALTER TABLE `tbl_orderlist`
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `tbl_pembeli`
--
ALTER TABLE `tbl_pembeli`
  ADD PRIMARY KEY (`pembeli_id`);

--
-- Indexes for table `tbl_produk`
--
ALTER TABLE `tbl_produk`
  ADD PRIMARY KEY (`produk_id`),
  ADD KEY `toko_id` (`toko_id`);

--
-- Indexes for table `tbl_toko`
--
ALTER TABLE `tbl_toko`
  ADD PRIMARY KEY (`toko_id`);

--
-- Indexes for table `tbl_wishlist`
--
ALTER TABLE `tbl_wishlist`
  ADD KEY `pembeli_id` (`pembeli_id`),
  ADD KEY `produk_id` (`produk_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_pembeli`
--
ALTER TABLE `tbl_pembeli`
  MODIFY `pembeli_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_produk`
--
ALTER TABLE `tbl_produk`
  MODIFY `produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `tbl_toko`
--
ALTER TABLE `tbl_toko`
  MODIFY `toko_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD CONSTRAINT `tbl_cart_ibfk_1` FOREIGN KEY (`pembeli_id`) REFERENCES `tbl_pembeli` (`pembeli_id`),
  ADD CONSTRAINT `tbl_cart_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `tbl_produk` (`produk_id`);

--
-- Constraints for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD CONSTRAINT `tbl_order_ibfk_1` FOREIGN KEY (`pembeli_id`) REFERENCES `tbl_pembeli` (`pembeli_id`);

--
-- Constraints for table `tbl_orderlist`
--
ALTER TABLE `tbl_orderlist`
  ADD CONSTRAINT `tbl_orderlist_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tbl_order` (`order_id`);

--
-- Constraints for table `tbl_produk`
--
ALTER TABLE `tbl_produk`
  ADD CONSTRAINT `tbl_produk_ibfk_1` FOREIGN KEY (`toko_id`) REFERENCES `tbl_toko` (`toko_id`);

--
-- Constraints for table `tbl_wishlist`
--
ALTER TABLE `tbl_wishlist`
  ADD CONSTRAINT `tbl_wishlist_ibfk_1` FOREIGN KEY (`pembeli_id`) REFERENCES `tbl_pembeli` (`pembeli_id`),
  ADD CONSTRAINT `tbl_wishlist_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `tbl_produk` (`produk_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
