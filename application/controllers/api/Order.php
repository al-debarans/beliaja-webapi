<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Order extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
    }

    function index_get(){

        $pembeli_id = $this->get('pembeli_id');

        if($pembeli_id != null || $pembeli_id != '')
        {
            $order = $this->db->get_where('tbl_order', 
                [
                    'pembeli_id' => $pembeli_id,
                    'pay_status' => 0

                ])->result();

            $this->response( array('order' => $order), 200);
        }
        else
        {
            $this->response( array('response' => 'fail'), 400);
        }     
    }

    function add_post(){

        $pembeli_id = $this->post('pembeli_id');
        $order_key = "ORD" . $pembeli_id . date('YmdHis');

        $this->db->select(
            'tbl_cart.produk_id,
             tbl_cart.jumlah,
             tbl_cart.notes,
             tbl_produk.produk_harga');
        $this->db->join('tbl_produk', 'tbl_produk.produk_id = tbl_cart.produk_id', 'inner');
        $this->db->where( array('pembeli_id' => $pembeli_id, 'status_prod' => 1));

        $cart = $this->db->get('tbl_cart')->result();

        if(sizeof($cart) != null || sizeof($cart) != 0)
        {
            $data = array(
                'order_id'      => $order_key,
                'pembeli_id'    => $pembeli_id,
                'pay_status'    => 0,
                'created'       => date('Y-m-d H:i:s')
            );
    
            $insert = $this->db->insert('tbl_order', $data);

            if($insert)
            {

                foreach ($cart as $row)
                {
                    $produk_id  = $row->produk_id;
                    $jumlah     = $row->jumlah;
                    $notes      = $row->notes;
                    $harga      = $row->produk_harga;

                    $total      = $jumlah * str_replace('.','',$harga);

                    $data = array(
                        'order_id'      => $order_key,
                        'produk_id'     => $produk_id,
                        'jumlah'        => $jumlah,
                        'notes'         => $notes,
                        'total'         => $total
                    );

                    $insert = $this->db->insert('tbl_orderlist', $data);

                    if($insert)
                    {
                        $this->reset_cart($pembeli_id);
                        $this->response( array('response' => 'success'), 201 );
                    }
                    else{
                        $this->response( array('response' => 'fail'), 501 );
                    }
                }
            }
        }     
    }

    function pay_post(){
        $pembeli_id = $this->post('pembeli_id');
        $order_id   = $this->post('order_id');

        $data = array(
            'pay_status' => 1
        );
        
        $this->db->where( array('pembeli_id' => $pembeli_id, 'order_id' => $order_id));
        $update = $this->db->update('tbl_order', $data);

        if($update)
        {
            $this->response( array('response' => 'success'), 201 );
        }
        else{
            $this->response( array('response' => 'fail'), 501 );
        }

    }

    function list_post()
    {
        $order_id = $this->post('order_id');

        $this->db->select(
            'tbl_orderlist.order_id,
             tbl_orderlist.jumlah,
             tbl_produk.produk_nama,
             tbl_produk.produk_pict,
             tbl_order.created');
        $this->db->join('tbl_produk', 'tbl_orderlist.produk_id = tbl_produk.produk_id', 'inner');
        $this->db->join('tbl_order', 'tbl_orderlist.order_id = tbl_order.order_id', 'inner');

        $this->db->where('tbl_orderlist.order_id',$order_id);

        $orderlist = $this->db->get('tbl_orderlist')->result();

        $this->response( array('orderlist' => $orderlist), 200);
    }

    function history_get(){
        $pembeli_id = $this->get('pembeli_id');

        if($pembeli_id != null || $pembeli_id != '')
        {
            $order = $this->db->get_where('tbl_order', 
                [
                    'pembeli_id' => $pembeli_id,
                    'pay_status' => 1

                ])->result();

            $this->response( array('order' => $order), 200);
        }
        else
        {
            $this->response( array('response' => 'fail'), 400);
        }
    }

    function reset_cart($pembeli_id){

        $pembeli_id = $this->post('pembeli_id');
        $notes = "";
        
        $data = array(
            'jumlah'        => 1,
            'notes'         => $notes,
            'status_prod'   => 0
        );
        $this->db->where( array('pembeli_id' => $pembeli_id, 'status_prod' => 1));
        $update = $this->db->update('tbl_cart', $data);
    }    
}