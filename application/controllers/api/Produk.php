<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Produk extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
    }

    function index_get(){

        $produk_id = $this->get('produk_id');
        $toko_id = $this->get('toko_id');
        $pembeli_id = $this->get('pembeli_id');
        $produk_nama = $this->get('produk_nama');

        if(($produk_id != null || $produk_id != '') && ($pembeli_id != null || $pembeli_id != ''))
        {
            $produk = $this->db->get_where('tbl_produk', array('produk_id' => $produk_id))->result();
            $toko = $this->db->get_where('tbl_toko', array('toko_id' => $toko_id))->result();
            
            $wishlist = $this->db->get_where('tbl_wishlist', array(
                'pembeli_id' => $pembeli_id,
                'produk_id' => $produk_id))->result();
            
            $this->response( array('produk' => $produk, 'toko' => $toko, 'wishlist' => $wishlist), 200);            
        } 
        
        else if($produk_nama != null || $produk_nama != '')
        {
            $this->db->like('produk_nama', $produk_nama);
            $produk = $this->db->get('tbl_produk')->result();
            $this->response( array('produk' => $produk), 200);
            
        }
        
        else {
            
            $this->db->from('tbl_produk');
            $this->db->order_by('produk_created', 'asc');
            $produk = $this->db->get()->result();

            $this->response( array('produk' => $produk), 200);
        }
    }
}