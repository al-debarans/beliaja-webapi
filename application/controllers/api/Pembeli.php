<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Pembeli extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
    }

    function register_post()
    {
        // get data post
        $pembeli_nama = $this->post('pembeli_nama');
        $pembeli_email = $this->post('pembeli_email');
        $pembeli_password = password_hash($this->post('pembeli_password'), PASSWORD_DEFAULT);
        $pembeli_hp = $this->post('pembeli_hp');
        $pembeli_alamat = $this->post('pembeli_alamat');
    
        // query pengecekkan email
        $this->db->where('pembeli_email', $pembeli_email);
        $users = $this->db->get('tbl_pembeli')->result();
        
        // kondisi pengecekkan android id
        // jika android id belum terdaftar masuk pada bagian else dan melakukan insert
        if($users)
        {
            
            $this->response( array('response' => 'fail'), 409 );

        } else{
            $data = array(
                'pembeli_nama'      => $pembeli_nama,
                'pembeli_email'     => $pembeli_email,
                'pembeli_password'  => $pembeli_password,
                'pembeli_hp'        => $pembeli_hp,
                'pembeli_alamat'    => $pembeli_alamat,
                'pembeli_created'   => date('Y-m-d H:i:s'),
                'pembeli_updated'    => date('Y-m-d H:i:s')
            );
    
            $insert = $this->db->insert('tbl_pembeli', $data);
    
            if($insert)
            {
                $this->db->where('pembeli_email', $pembeli_email);
                $pembeli = $this->db->get('tbl_pembeli')->result();
                $this->response( array('response' => 'success', 'pembeli' => $pembeli), 201 );
            } else
            {
                $this->response( array('response' => 'fail'), 401);
            }
        }
    }

    function login_post(){
        $pembeli_email = $this->post('pembeli_email');
        $pembeli_password = $this->post('pembeli_password');

        $pembeli = $this->db->get_where('tbl_pembeli', ['pembeli_email' => $pembeli_email])->result();

        if(sizeof($pembeli) != null || sizeof($pembeli) != 0)
        {
            foreach ($pembeli as $row)
            {
                $email = $row->pembeli_email;
                $password = $row->pembeli_password;
            }

            if(password_verify($pembeli_password , $password)){
                $this->response( array('response' => 'success', 'profile' => $pembeli), 200 );
            }
            else{
                $this->response( array('response' => 'fail'), 401 );
            }

        }
        else{
            $this->response( array('response' => 'fail'), 401 );
        }
    }

    function profile_get(){

        $pembeli_email = $this->get('pembeli_email');

        if($pembeli_email != null || $pembeli_email != ''){

            $pembeli = $this->db->get_where('tbl_pembeli', array('pembeli_email' => $pembeli_email))->result();
            $this->response( array('profile' => $pembeli), 200);
        }
        else{

            $this->response( array('response' => 'fail'), 404 );
        }
    }

}