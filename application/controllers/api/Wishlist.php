<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Wishlist extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
    }

    function index_get()
    {
        $pembeli_id = $this->get('pembeli_id');

        if($pembeli_id != null || $pembeli_id != '')
        {
            $this->db->select(
                'tbl_wishlist.pembeli_id, 
                tbl_wishlist.produk_id,
                tbl_toko.toko_id, 
                tbl_toko.toko_nama, 
                tbl_toko.toko_lokasi, 
                tbl_produk.produk_nama, 
                tbl_produk.produk_harga, 
                tbl_produk.produk_pict'
            );
            $this->db->join('tbl_produk', 'tbl_produk.produk_id = tbl_wishlist.produk_id', 'inner');
            $this->db->join('tbl_toko', 'tbl_toko.toko_id = tbl_produk.toko_id', 'inner');
            $this->db->where('pembeli_id', $pembeli_id);

            $wishlist = $this->db->get('tbl_wishlist')->result();

            $this->response( array('wishlist' => $wishlist), 200 );
        }

        else{
            $this->response( array('response' => 'fail'), 400 );
        }

    }

    function wishlist_post(){

        $pembeli_id = $this->post('pembeli_id');
        $produk_id = $this->post('produk_id');

        // query pengecekkan wishlist_id
        $wishlist = $this->db->get_where('tbl_wishlist', 
                [
                    'pembeli_id' => $pembeli_id,
                    'produk_id' => $produk_id

                ])->result();

        if(sizeof($wishlist) == null || sizeof($wishlist) == 0){
            // jika pada tabel wishlist data belum ada maka melakukan insert terlebih dahulu
            $data = array(
                'pembeli_id'     => $pembeli_id,
                'produk_id'     => $produk_id,
                'created'       => date('Y-m-d H:i:s')
            );
    
            $insert = $this->db->insert('tbl_wishlist', $data);
            $this->response( array('response' => 'success'), 201 );
        }

        else
        {
            $this->response( array('response' => 'fail'), 501 );
        }            
    }

    function unwishlist_post(){

        $pembeli_id = $this->post('pembeli_id');
        $produk_id = $this->post('produk_id');

        $this->db->where( array('pembeli_id' => $pembeli_id, 'produk_id' => $produk_id));
        
        $delete = $this->db->delete('tbl_wishlist');

        if($delete)
        {
            $this->response( array('response' => 'success'), 201 );
        }
        else{
            $this->response( array('response' => 'fail'), 501 );
        }

    }
}