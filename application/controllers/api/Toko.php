<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Toko extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
    }

    function profile_get(){

        $toko_id = $this->get('toko_id');

        if($toko_id != null || $toko_id != ''){

            $toko = $this->db->get_where('tbl_toko', array('toko_id' => $toko_id))->result();
            $produk = $this->db->get_where('tbl_produk', array('toko_id' => $toko_id))->result();
            $this->response( array('toko' => $toko, 'produk' => $produk), 200);
        }
        else{

            $this->response( array('response' => 'fail'), 404 );
        }
    }
}