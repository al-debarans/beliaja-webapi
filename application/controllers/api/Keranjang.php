<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Keranjang extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
    }

    function index_get()
    {
        $pembeli_id = $this->get('pembeli_id');
        $status_prod = $this->get('status_prod');
        if($status_prod == 'true'){
            
            $status_prod = 1;
            $this->db->select(
                'tbl_cart.cart_id, 
                tbl_cart.produk_id,
                tbl_cart.jumlah,
                tbl_cart.notes,
                tbl_cart.status_prod,
                tbl_toko.toko_id, 
                tbl_toko.toko_nama,
                tbl_produk.produk_nama, 
                tbl_produk.produk_harga, 
                tbl_produk.produk_pict'
            );
            $this->db->join('tbl_produk', 'tbl_produk.produk_id = tbl_cart.produk_id', 'inner');
            $this->db->join('tbl_toko', 'tbl_toko.toko_id = tbl_produk.toko_id', 'inner');
            $this->db->where( array('pembeli_id' => $pembeli_id, 'status_prod' => $status_prod));

            $cart = $this->db->get('tbl_cart')->result();

            $this->response( array('keranjang' => $cart), 200 );

        }

        else{
            $this->response( array('response' => 'fail'), 400 );
        }

    }

    function tambah_post(){

        $pembeli_id = $this->post('pembeli_id');
        $produk_id = $this->post('produk_id');
        $jumlah = $this->post('jumlah');
        $notes = $this->post('notes');

        // query pengecekkan cart
        $cart = $this->db->get_where('tbl_cart', 
                [
                    'pembeli_id' => $pembeli_id,
                    'produk_id' => $produk_id

                ])->result();

        if(sizeof($cart) == null || sizeof($cart) == 0){
            // jika pada tabel wishlist data belum ada maka melakukan insert terlebih dahulu
            $data = array(
                'pembeli_id'    => $pembeli_id,
                'produk_id'     => $produk_id,
                'jumlah'        => $jumlah,
                'notes'         => $notes,
                'date_use'      => date('Y-m-d H:i:s'),
                'status_prod'   => 1
            );
    
            $insert = $this->db->insert('tbl_cart', $data);
            if($insert)
            {
                $this->response( array('response' => 'success'), 201 );
            } else
            {
                $this->response( array('response' => 'fail'), 501);
            }
        }
        else
        {
            $data = array(
                'jumlah'        => $jumlah,
                'notes'         => $notes,
                'status_prod'   => 1, 
                'date_use'      => date('Y-m-d H:i:s')
            );
            $this->db->where( array('pembeli_id' => $pembeli_id, 'produk_id' => $produk_id));
            $update = $this->db->update('tbl_cart', $data);

            if($update)
            {
                $this->response( array('response' => 'success'), 201 );
            } else{
                $this->response( array('response' => 'fail'), 501 );
            }
        }            
    }

    function hapus_post(){

        $pembeli_id = $this->post('pembeli_id');
        $produk_id = $this->post('produk_id');
        $notes = "";
        
        $data = array(
            'jumlah'        => 1,
            'notes'         => $notes,
            'status_prod'   => 0
        );
        $this->db->where( array('pembeli_id' => $pembeli_id, 'produk_id' => $produk_id));
        $update = $this->db->update('tbl_cart', $data);

        if($update)
        {
            $this->response( array('response' => 'success'), 201 );
        }
        else{
            $this->response( array('response' => 'fail'), 501 );
        }

    }


}